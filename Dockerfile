FROM alpine

RUN apk add python3 py3-pip make git

COPY requirements.txt /tmp/requirements.txt
RUN pip install -r /tmp/requirements.txt
