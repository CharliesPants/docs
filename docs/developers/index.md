# Why Mosaic?

If you are an application developer, you might be wondering why you should package your application for Mosaic or why you should offer it on our App store.

## Why we believe in web apps

Every operating system vendor is trying to create their own walled garden of languages and frameworks to entice companies and developers to create applications that are tied in to their system. This has been reasonably successful in the past.

One of the thing that we have been observing for years is the growth of the web as a platform. Web applications are by nature cross-platform. The technologies that the Web offers is constantly growing: WebAssembly, WebGPU, WebDB, WebWorkers are just a few standards that have been accepted in the past couple years. 

Security is another consideration where the Web has advantages. While there are still vulnerabilities once in a while, web security is something that has been well-studied, and today Browsers are some of the best-sandboxed environments.

Finally, freedom. Everything that Mosaic does is open source, meaning that you are not tied to our platform. Web technology is open source, too. 

In short, we believe that if you want to write an application in 2022, the Web is the most promising platform.

## Why you should choose self-hosting

As an application developer, you have many choices on where and how to deploy the backend of your application. If you host and deploy it, it means that you take all of the (legal) responsibility of keeping the data safe, keeping the backend up and running, making sure that all parts of it are designed to scale, monitoring it, analyzing the logs for anomalies, combatting spam accounts, defending against hacking attempts, keeping the uptime high, and with all of that going on, making sure that your hosting costs are not eating away too much of your profit with upcoming rises of the cost of cloud computing.

What Mosaic offers you is not just an alternative way of deploying your application, but also an alternative way of developing your application. Apps on Mosaic are not expected to scale to millions of users: typically, a single instance of an application is used by a dozen or so users. This means that you do not need to put so much effort into making it secure, multi-tenant, resilient against spam and all such. It effectively means that you can cut your development cost in half. And keep in mind that you do not have to spend anything on the cloud, either, as the users have their own hardware.

We try to give developers as much tools as we can for developing applications. For example, if you do not want to bother writing an authentication system, you can use one that we provide for no charge.

## Sustainable Software

The final point to make is that we are trying to promote sustainable software. Software lives from both it's users and the developer: it solves some problem that users have, who in return award the developers of the software. Software is also rarely perfect or complete, it is more of a path. As such, the journey does not end when a particular application is purchased.

Our system, which we believe encourages sustainable development, is that updates for software can be set to cost some fee. This means that developers keep on getting paid for enhancing their software, but unlike with a subscription model, users have ownership of their software. Using microtransactions, even software that is updated and deployed very frequently can generate an income stream for users. By users being able to choose whether or not to purchase updates, they effectively cast a vote on whether or not they think development is going into a good direction, encouraging quality software on the platform.

But user and developer interaction does not end there. Our plans for the platform include a crowdfunding ability, where users can come together to back whole new applications or new features for existing applications.

## Conclusion

We believe that Mosaic is more than just a self-hosted application runtime, we believe that it is a platform for the future. 

