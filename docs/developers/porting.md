# Packaging your Application for Mosaic

Mosaic applications are built on Docker containers. An application is thus defined as a
docker-compose file. Here's an example of how the application docker-compose looks like for
[Element](https://element.io), a Matrix-based chat application we support:

```yaml
version: '3.9'
services:
    nginx:
        image: fractalnetworks/element-nginx-proxy:latest
        environment:
            LINK_DOMAIN_DEFAULT: $LINK_DOMAIN_DEFAULT
    element:
        image: vectorim/element-web:latest
    synapse:
        image: matrixdotorg/synapse:latest
        environment:
            SYNAPSE_SERVER_NAME: $LINK_DOMAIN_DEFAULT
            SYNAPSE_REPORT_STATS: "no"
        entrypoint: "bash -c 'python /start.py generate; python /start.py'"
        volumes:
            - fractal-app-volume:/data
    link:
      image: fractalnetworks/link:latest
      environment:
        LINK: $LINK_DOMAIN_DEFAULT
        TOKEN: $LINK_TOKEN_DEFAULT
        EXPOSE: nginx:80
        FRACTAL_API: https://gateway.fractalnetworks.co
      cap_add:
        - NET_ADMIN
      volumes:
        - fractal-link-volume:/root/.local/share/caddy
volumes:
  fractal-app-volume:
    external: true
    name: $APP_VOLUME_NAME
  fractal-link-volume:
    external: true
    name: $LINK_VOLUME_NAME
```

In this file, you can find two unusual things:

- The `link` container. This is a docker container that provides incoming connectivity
  for the application by connecting to a WireGuard VPN and receiving end-to-end encrypted
  connection from the outside. It uses [Let's Encrypt](https://letsencrypt.org) to provision
  free, short-lived TLS certificates for itself. It is configured to point to the `nginx` container,
  and it takes a `$LINK_DOMAIN_DEFAULT` and `$LINK_TOKEN_DEFAULT` env variables, which is filled
  in by the Hive device.
- The volumes, which are declared as external volumes. This is where the Fractal Storage volumes
  are used, which implement automatic snapshotting and with that replication in an end-to-end
  encrypted manner.

## Instructions

If you are able to build a Docker image with your application (or a few Docker images, for example
if and when you need external tools, such as databases), then it is very easy to convert it into
an application that can run on Mosaic.

What you have to keep in mind is that your application will receive HTTP connections from the link
container, not HTTPS. Mosaic works best if you let the link container handle all of the TLS termination
for you. It uses `Caddy` to do so, which can be configured to support a lot of modern protocols if you
need, such as HTTP/2 or WebSockets.

The only think you need to worry about is making sure that all of your application state is kept in
volumes, so that it can be snapshotted and replicated.

When you publish updates for your application, you must make sure that it will work with data from old
versions of the app. This is for example accomplished by running migrations automatically at startup
to make sure that the database is in working order.


