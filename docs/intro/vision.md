# Vision

We are a small team of four developers working together on the shared goal of making self-hosting effortless for everyone
by building software that automates the hard parts.

We all self-host software and we enjoy doing so, but we also know the pain involved with manually managing,
monitoring and deploying software. We want to build something that combines the reliability of commercial applications
with running in the safety and security of the home.

Further than that, we are trying to create an ecosystem through our app store, where users can find the applications
they want or need, and developers can build and maintain those applications while making a living from it.

Our goal is to create the *App Store for the Internet*.

## Our Strategy

Support all good faith efforts to return control of the internet to an user-owned majority.
