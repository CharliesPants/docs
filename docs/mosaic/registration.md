# Registration

If you open up our hosted Mosaic instance on [console.fractalnetworks.co](https://console.fractalnetworks.co), you should be greeted by this login screen:

![Mosaic Login Screen](../media/mosaic-login-empty.png)

On the bottom right of that little login window, there is a link to *Register*. Click on that link, which will take you to the registration
form.

![Mosaic Registration Form](../media/mosaic-registration.png)

In that registration form, fill in your name and email address. You have to choose a username, which can be anything, and a secure password. We recommend you to use a passphrase.

Once you have put in all of your details, click on the big blue Register button on the bottom of that window, which will take you back to the login window.

![Mosaic Login Window](../media/mosaic-login-prefilled.png)

Back in the login windo, fill in your email address (or user name) and the passphrase you have previously chosen, and then click the big blue Sign In button on the bottom

With that done, you should be logged in. Continue to the next page to get an overview of what you can do.
