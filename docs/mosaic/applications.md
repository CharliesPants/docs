---
title: Applications
---

# Applications

The Fractal Mosaic App Catalog comes with a small collection of **Featured Services**. For now, supporting a small
selection of applications means that we can focus on improving the reliability of the system.

Drop by our [Matrix channel](https://matrix.to/#/#fractal:ether.ai) or open an issue on
[GitHub](https://github.com/fractalnetworksco/app-catalog/issues) if you'd like us to add your favorite self-hosted
applications to Mosaic!

## Migrating An Existing App

Fractal Mosaic applications are simply [Docker Compose](https://docs.docker.com/compose/) files. Therefore, migrating your
existing applications to take advantage of Mosaic's automated backups, secure connectivity, and rescheduling
capabilities is simple if you are already familiar Docker. Visit the developer documentation for more information
on how to [migrate existing applications to Mosaic](developers).

## Installing Apps

When logged into the Console, Fractal Mosaic's *Featured Services* are presented.

<figure markdown>
![Empty Console](../media/mosaic-empty.png){ width="500" }
  <figcaption>Console Without Any Deployed Applications</figcaption>
</figure>

If you select any one of the *Featured Services* and click the *Deploy* button, a dialog will open
asking you to select a device to deploy the application on.

<figure markdown>
![App Deploy Dialog](../media/mosaic-app-deploy.png){ width="500" }
  <figcaption>Application Deployment Dialog</figcaption>
</figure>

Feel free to select or add any device you like. Once you have selected a device, the application will be started on your
chosen device. At this point, your application is displayed inside the Console.

!!! note "Fractal Cloud Device Offering"
    The *Fractal Cloud Device* is a *potential offering* where we will run your
    application in the cloud, if you want to try Mosaic without setting up your own device. These devices are not currently
    provided, but if you would like the ability to run one, leave a message in our
    [Matrix channel](https://matrix.to/#/#fractal:ether.ai) or open an issue on
    [GitHub](https://github.com/fractalnetworksco/app-catalog/issues).

<figure markdown>
![App Deployed](../media/mosaic-app-deployed.png){ width="500" }
  <figcaption>Application Deployed in Console</figcaption>
</figure>

Note that the app's health is currently *red*, meaning that your device has not confirmed
that the app is running yet. After a few moments, your app's health should change
to green, indicating that the app is now running and is accessible at the link in the card.

<figure markdown>
![App Running Healthy](../media/mosaic-app-deployed-green.png){ width="500" }
  <figcaption>Running Application Healthy</figcaption>
</figure>

> Most applications will require some first time setup which varies between the applciation

<figure markdown>
![Example App Requiring Initial Configuration](../media/mosaic-wikijs-running.png){ width="500" }
  <figcaption>Example App Requiring Initial Configuration</figcaption>
</figure>

## Configuring Apps

Installed applications in the Console offer a variety of actions that can be taken:

- Uninstall
  - Removes the application from your device. This also removes the data for the app.
- Reschedule
  - Marks the app to be rescheduled to another device of your choosing.
<!--FIXME!!!!-->
- Application Specific Configuration
  - Specific app configuration that can be applied to the app. [See Element App Configuration](fix-me)

<figure markdown>
![App Reschedule Dialog](../media/mosaic-app-reschedule.png){ width="500" }
  <figcaption>Console Application Reschedule Dialog</figcaption>
</figure>
