---
title: Hive
---

!!! warning "WIP"

    **This document is a work in progress and is subject to change.**

The crux of Fractal Mosaic is application orchestration. Hive is the privacy respecting orchestrator for your
self-hosted applications. It can schedule your applications and apply various other app-specific commands to your
connected devices.

## Managing Applications

### Scheduling and Failover

Being able to schedule and failover an application is an import part of Fractal Mosaic. Hence, Hive Backend's main role is
scheduling your application. The scheduler is responsible for determining when start and stop messages should be sent to
your devices.

#### Starting Applications

<figure markdown>
![Mosaic Deploy Application](../media/mosaic-app-deploy.png){ width="500" }
  <figcaption>Deploying an Application in the Console</figcaption>
</figure>

The Hive scheduler will send a start message to your device containing context about the app you have chosen to
install. Upon receiving this message, the device will begin starting the app and once started will report the app in
its list of running apps. When Hive receives this message, Hive will report your app as healthy.

<figure markdown>
![Mosaic Healthy Application](../media/mosaic-app-deployed-green.png){ width="500" }
  <figcaption>Healthy Application in the Console</figcaption>
</figure>

For more information on specifically what your Device is doing when starting an app,
[see Device documentation](https://docs.fractalnetworks.co/mosaic/devices/#starting-an-app).

#### Stopping Applications

When your choose to stop an app, the scheduler will send a stop message to your device. After the device successfully
stops the app, the app will not longer be reported in the device's running app list. As a result, Hive will then update
the app's health to offline (red).

<figure markdown>
![Mosaic Offline Application](../media/mosaic-app-deployed.png){ width="500" }
  <figcaption>Offline Application in the Console</figcaption>
</figure>

#### Rescheduling Applications

One of the main features offered by Fractal Mosaic is the ability to reschedule an application between your devices. Hive's
scheduler supports simple rescheduling (with automated failover coming later). When the user chooses to reschedule
an app to another device:

1. Hive sends a stop message to the Device 1.
1. After the Device 1 has confirmed that the app is stopped, Hive sends a start message to Device 2
1. Device 2 starts the app using the device encrypted private keys.

#### Application Metadata

When Hive sends a message to your device, metadata is provided to give the device all of the context it needs. This
metadata includes:

- Basic info about the app:
  - App's name
  - App version (set to `latest` for now)
- The publicly accessible domain the app will be at (ex. *hasty-hopper.fractalnetworks.co*)
- Device Passphrase encrypted storage private keys (or None -- indicates to the device that it should generate them))
    > Your device will generate storage private keys if there are none stored, and send the passphrase encrypted
      version to the backend in order for failover to work seamlessly between your devices.
- Device Command -- What action should the device take.

## Device Connection

Your device maintains a connection with Hive in order to receive commands and so that Fractal can inform you when your
device loses connection or if any of your applications running on the device encounter errors. If your device loses
connectivity for more than a minute, then your device is considered unhealthy.

<figure markdown>
![Mosaic Offline Device](../media/mosaic-device-unhealthy.png){ width="400" }
  <figcaption>Offline Device in the Console</figcaption>
</figure>

## Notifications

Because your devices maintain a connection with Hive, notifications related to device health can be provided. As
mentioned in the [Device Connection](#device-connection) section above, Hive is able to detect when your device has
lost connection for over a minute. In this case, if you are subscribed to notifications, Hive will send you
an email notifying you about the outage.

Other supported notifications include:

- Start / Stop Confirmations for Apps
- Device Disk Space Running Low

Weekly Notifications

- Launch your First Device

## Privacy

Hive has been designed with Privacy in mind. Your private keys,
[as mentioned in the Device documentation](https://docs.fractalnetworks.co/mosaic/devices/#a-note-on-generated-storage-private-keys), are encrypted with the device passphrase on the device before being sent to
be stored in our database. Doing so means that Hive -- or anyone lacking your device passphrase -- cannot decrypt
your private keys.

Hive does not store any more information than it needs about you. What Hive stores:

- Your email address
  - Allows Hive to email you when your apps or devices' health change. *You have the option to disable notifications
    if preferred.*
- Hashed & salted device passphrase
  - Allows Hive to verify that any device you add in the Console has the same device passphrase set)
